package algo;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int streetCount = s.nextInt();
        s.nextLine();

        for (int i = 0; i < streetCount; i++) {
            int price = s.nextInt();
            s.nextLine();
            String hSeq = s.nextLine();
            int currMaxHeight = 0;
            int canSeeCount = 0;
            for (String h : hSeq.split(" ")) {
                int height = Integer.valueOf(h);
                if (currMaxHeight < height) {
                    canSeeCount++;
                    currMaxHeight = height;
                }
            }
            System.out.println(canSeeCount * price);
        }
    }
}
