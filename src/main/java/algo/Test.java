package algo;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Test {
    public static void main(String args[]) throws Exception {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        Set<Integer> unique = new HashSet<>();
        Set<Integer> repeated = new HashSet<>();
        int[] a = new int[n];
        int[] cumulative = new int[n];
        for (int i = 0; i < n; i++) {
            int el = s.nextInt();
            a[i] = el;
            if (unique.contains(el)) {
                unique.remove(el);
                repeated.add(el);
                cumulative[i] = unique.size() - 1;
            }
            else {
                if (!repeated.contains(el)) {
                    unique.add(el);
                }
                cumulative[i] = unique.size();
            }
        }
//        System.out.println(Arrays.toString(cumulative));
        int q = s.nextInt();
        for (int i = 0; i < q; i++) {
            int l = s.nextInt();
            int r = s.nextInt();
            int ans = cumulative[r-1] - cumulative[l-1];
            System.out.println(ans);
        }
    }
}
