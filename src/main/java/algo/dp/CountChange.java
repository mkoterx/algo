package algo.dp;

public class CountChange {

    public static void main(String[] args) {
        int ans = countChange(40, new int[]{1, 2, 5, 10, 25, 50});
        System.out.println(ans);
    }

    public static int countChange(int change, int[] coins) {
        int[] minNumCoins = new int[change+1];
        minNumCoins[0] = 0;
        for (int money = 1; money <= change; money++) {
            minNumCoins[money] = Integer.MAX_VALUE;
            for (int i = 0; i < coins.length; i++) {
                if (money >= coins[i]) {
                    int numCoins = minNumCoins[money - coins[i]] + 1;
                    if (numCoins < minNumCoins[money])
                        minNumCoins[money] = numCoins;
                }
            }
        }
        return minNumCoins[change];
    }
}
