package algo.dp;

import java.util.Objects;
import java.util.Scanner;

public class EditDist {

    static final int MATCH = 0;
    static final int INSERT = 1;
    static final int DELETE = 2;

    static final class Cell {
        public int cost;
        public int parent;

        public Cell() {
        }

        Cell(int cost, int parent) {
            this.cost = cost;
            this.parent = parent;
        }
    }

    static final class Pair {
        public final int x;
        public final int y;

        Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    static Cell[][] m;

    static int strcmp(String s1, String s2) {
        s1 = " " + s1;
        s2 = " " + s2;
        char[] s = s1.toCharArray();
        char[] t = s2.toCharArray();

        m = new Cell[s.length][t.length];

        int[] opt = new int[3];

        for (int i = 0; i < s.length; i++) col_init(i);
        for (int i = 0; i < t.length; i++) row_init(i);

        for (int i = 1; i < s.length; i++)
            for (int j = 1; j < t.length; j++) {
                opt[MATCH] = m[i - 1][j - 1].cost + match(s[i], t[j]);
                opt[INSERT] = m[i][j - 1].cost + indel(t[j]);
                opt[DELETE] = m[i - 1][j].cost + indel(s[i]);
                m[i][j] = new Cell();
                m[i][j].cost = opt[MATCH];
                m[i][j].parent = MATCH;
                for (int k = INSERT; k <= DELETE; k++)
                    if (opt[k] < m[i][j].cost) {
                        m[i][j].cost = opt[k];
                        m[i][j].parent = k;
                    }
            }
        Pair p = goal_cell(s, t);
        return (m[p.x][p.y].cost);

    }

    private static void reconstruct_path(char[] s, char[] t, int i, int j) {
        if (m[i][j].parent == -1) return;
        if (m[i][j].parent == MATCH) {
            reconstruct_path(s, t, i - 1, j - 1);
            match_out(s[i], t[i]);
        }
        if (m[i][j].parent == INSERT) {
            reconstruct_path(s, t, i, j - 1);
            System.out.print("I");
        }
        if (m[i][j].parent == DELETE) {
            reconstruct_path(s, t, i - 1, j);
            System.out.print("D");
        }
    }

    private static void match_out(char s, char t) {
        if (Objects.equals(s, t)) System.out.print("M");
        else System.out.print("S");
    }

    private static int match(char c, char d) {
        if (c == d) return 0;
        else return 1;
    }

    private static int indel(char c) {
        return 1;
    }

    private static Pair goal_cell(char[] s, char[] t) {
        return new Pair(s.length - 1, t.length - 1);
    }

    private static void row_init(int i) {
        m[0][i] = new Cell();
        m[0][i].cost = i;
        if (i > 0)
            m[0][i].parent = INSERT;
        else
            m[0][i].parent = -1;
    }

    private static void col_init(int i) {
        m[i][0] = new Cell();
        m[i][0].cost = i;
        if (i > 0)
            m[i][0].parent = DELETE;
        else
            m[0][i].parent = -1;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String s1 = s.nextLine();
        String s2 = s.nextLine();
        int result = strcmp(s1, s2);
        System.out.println(result);

        s1 = " " + s1;
        s2 = " " + s2;
        char[] a = s1.toCharArray();
        char[] b = s2.toCharArray();

        reconstruct_path(a, b,a.length - 1, b.length -1);
    }


}
