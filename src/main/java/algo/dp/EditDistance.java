package algo.dp;

class EditDistance {

    static int calc_min_edit_dist(char[] word1, char[] word2) {
        int n = word1.length + 1;
        int m = word2.length + 1;
        int[][] D = fill_matrix(word1, word2, n, m);
        return D[n - 1][m - 1];
    }

    public static int count_ways(char[] word1, char[] word2) {
        int n = word1.length + 1;
        int m = word2.length + 1;

        int[][] D = fill_matrix(word1, word2, n, m);

        int[][] ways = new int[n][m];
        for (int row = 0; row < n; row++) ways[row][0] = 1;
        for (int col = 0; col < m; col++) ways[0][col] = 1;

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                if (D[i][j] == D[i - 1][j] + 1)
                    ways[i][j] += ways[i - 1][j];
                if (D[i][j] == D[i][j - 1] + 1)
                    ways[i][j] += ways[i][j - 1];
                if (word1[i - 1] == word2[j - 1] && D[i][j] == D[i - 1][j - 1])
                    ways[i][j] += ways[i - 1][j - 1];
                if (D[i][j] == D[i - 1][j - 1] + 1)
                    ways[i][j] += ways[i - 1][j - 1];
            }
        }
        return ways[n - 1][m - 1];
    }

    /**
     *
     * @param word1
     * @param word2
     * @param n matrix height
     * @param m matrix width
     * @return
     */
    private static int[][] fill_matrix(char[] word1, char[] word2, int n, int m) {
        int[][] D = new int[n][m];

        for (int row = 0; row < n; row++) D[row][0] = row;
        for (int col = 0; col < m; col++) D[0][col] = col;

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                int insertion = D[i][j - 1] + 1;
                int deletion = D[i - 1][j] + 1;
                int mismatch = D[i - 1][j - 1] + 1;
                int match = D[i - 1][j - 1];
                if (word1[i - 1] == word2[j - 1])
                    D[i][j] = min(insertion, deletion, match);
                else
                    D[i][j] = min(insertion, deletion, mismatch);
            }
        }
        return D;
    }

    private static int min(int a, int b, int c) {
        return Math.min(a, Math.min(b, c));
    }
}
