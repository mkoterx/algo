package algo.dp;

/**
 * Longest Common Subarray
 */
class Lcs {

    static int calc_lcs_len(char[] A, char[] B) {
        int n = A.length + 1;
        int m = B.length + 1;
        int[][] D = new int[n][m];
        for (int row = 0; row < n; row++) D[row][0] = 0;
        for (int col = 0; col < m; col++) D[0][col] = 0;

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                if (A[i-1] == B[j-1])
                    D[i][j] = D[i-1][j-1] + 1;
                else
                    D[i][j] = Math.max(D[i][j-1], D[i-1][j]);
            }
        }
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < m; j++)
//                System.out.print(D[i][j] + " ");
//            System.out.println();
//        }

        return D[n-1][m-1];
    }

    static int calc_lcs_len_rec(char[] A, char[] B) {
        int n = A.length + 1;
        int m = B.length + 1;
        int[][] C = new int[n][m];

        for (int row = 0; row < n; row++) C[row][0] = 0;
        for (int col = 0; col < m; col++) C[0][col] = 0;

        for (int i = 1; i < n; i++)
            for (int j = 1; j < m; j++)
                C[i][j] = -1;

        return calc_lcs_len_rec(A, n-1, B, m-1, C);
    }

    private static int calc_lcs_len_rec(char[] A, int i, char[] B, int j, int[][] C) {
        if (i == 0 || j == 0)
            return C[i][j];
        if (A[i-1] == B[j-1]) {
            int res = 1 + calc_lcs_len_rec(A, i-1, B, j-1, C);
            C[i][j] = res;
            return res;
        } else {
            int max1;
            if (C[i-1][j] != -1) {
                max1 = C[i-1][j];
            } else {
                max1 = calc_lcs_len_rec(A, i-1, B, j, C);
            }

            int max2;
            if (C[i][j-1] != -1) {
                max2 = C[i][j-1];
            } else {
                max2 = calc_lcs_len_rec(A, i, B, j-1, C);
            }

            int max = Math.max(max1, max2);
            C[i][j] = max;
            return max;
        }
    }
}
