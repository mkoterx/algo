package algo.hash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://www.geeksforgeeks.org/find-number-of-employees-under-every-manager/
 */
public class TotalEmployeesUnderEmployee {

    static Map<String, Integer> result = new HashMap<>();

    public static void main(String[] args) {
        Map<String, String> dataSet = new HashMap<>();
        dataSet.put("A", "C");
        dataSet.put("B", "C");
        dataSet.put("C", "F");
        dataSet.put("D", "E");
        dataSet.put("E", "F");
        dataSet.put("F", "F");

        populateResultMap(dataSet);
        System.out.println(result);
    }

    private static void populateResultMap(Map<String, String> dataSet) {
        Map<String, List<String>> mngrEmpMap = reverseMap(dataSet);
        for (String mngr : dataSet.keySet()) {
            int count = calcTotalEmployeesUnderManager(mngr, mngrEmpMap);
            result.put(mngr, count);
        }
    }

    private static Map<String, List<String>> reverseMap(Map<String, String> dataSet) {
        Map<String, List<String>> reverseMap = new HashMap<>();
        for (Map.Entry<String, String> entry : dataSet.entrySet()) {
            String emp = entry.getKey();
            String mngr = entry.getValue();
            if (!emp.equals(mngr)) {
                reverseMap.putIfAbsent(mngr, new ArrayList<>());
                reverseMap.computeIfPresent(mngr, (manager, employees) -> {
                    employees.add(emp);
                    return employees;
                });
            }
        }
        return reverseMap;
    }

    private static int calcTotalEmployeesUnderManager(String mngr, Map<String, List<String>> mngrEmpMap) {
        if (!mngrEmpMap.containsKey(mngr)) {
            return 0;
        } else if (result.containsKey(mngr)) {
            return result.get(mngr);
        } else {
            int count;
            List<String> directReportEmps = mngrEmpMap.get(mngr);
            count = directReportEmps.size();
            for (String directReportEmp : directReportEmps) {
                count += calcTotalEmployeesUnderManager(directReportEmp, mngrEmpMap);
            }
            return count;
        }
    }
}
