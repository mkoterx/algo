package algo.twopointer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MaxSubarraySum {

    public static void main(String[] args) {
        int[] a = {2, 2, 2, 2, 1, 2, 2, 3, 2, 2, 2, 4, 4, 1, 1, 5, 5, 6, 5};
        long M = 11;
//        long ans = mss_posi_steq(a, a.length, M);
        long ans = smallest(a, a.length, 5);
        System.out.println(ans);
    }

    /**
     * Problem:
     * Given an array having N positive integers, find the sum of contiguous subarray having sum as great as possible,
     * but not greater than M
     *
     * Constraints:
     * Array can have atmost 10^5 elements and each number will be non-negative and can be as big as 10^9.
     *
     * @param A array with positive numbers
     * @param n array length
     * @param M upper limit of the sum
     */
    public static long mss_posi_steq(int[] A, int n, long M) {
        int l = 0;
        int r = 0;
        long sum = 0;
        long ans = 0;

        while (l < n) {
            while (r < n && sum + A[r] <= M) {
                sum += A[r];
                r++;
            }
            ans = Math.max(ans, sum);
            sum -= A[l];
            l++;
        }
        return ans;
    }

    public static int smallest(int[] A, int n, int K) {
        int l = 0;
        int r = 0;
        int ans = Integer.MAX_VALUE;
        Map<Integer, Integer> map = new HashMap<>();
        Set<Integer> set = new HashSet<>();
        while (l < n) {
            while (r < n && set.size() < K) {
                set.add(A[r]);
                int cnt = map.getOrDefault(A[r], 0);
                map.put(A[r], cnt + 1);
                r++;
            }
            if (set.size() == K) ans = Math.min(ans, r - l);
            if (map.get(A[l]) == 1) set.remove(A[l]);
            int cnt = map.get(A[l]);
            map.put(A[l], cnt - 1);
            l++;
        }
        return Integer.compare(ans, Integer.MAX_VALUE) == 0 ? -1 : ans;
    }
}
