package algo.twopointer;

import java.util.Arrays;

public class MergeSortedArrays {

    public static void main(String[] args) {
        int[] a = {1, 3, 5, 6, 8};
        int[] b = {2, 4, 5, 7, 11};
        int[] c = merge(a, b);
        System.out.println(Arrays.toString(c));
    }

    public static int[] merge(int[] A, int[] B) {
        int n = A.length;
        int m = B.length;
        int[] C = new int[n + m];
        int l1 = 0;
        int l2 = 0;
        int pos = 0;
        while (l1 < n || l2 < m) {
            if (l1 < n && l2 < m) {
                if (A[l1] < B[l2]) C[pos++] = A[l1++];
                else C[pos++] = B[l2++];
            }
            else if (l1 < n) C[pos++] = A[l1++];
            else if (l2 < m) C[pos++] = B[l2++];
        }
        return C;
    }
}
