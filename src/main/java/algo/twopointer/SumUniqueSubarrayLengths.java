package algo.twopointer;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Given an array, calculate the sum of lengths of contiguous subarrays having all elements distinct.
 *
 * help link: https://www.geeksforgeeks.org/subarrays-distinct-elements/
 *
 * other links:
 * https://tp-iiita.quora.com/The-Two-Pointer-Algorithm
 */
class SumUniqueSubarrayLengths {

    public static void main(String args[]) throws Exception {
        Scanner s = new Scanner(System.in);
        long T = Integer.valueOf(s.nextLine());
        for (long i = 0; i < T; i++) {
            int N = Integer.valueOf(s.nextLine());
            int[] A = new int[N];
            for (int b = 0; b < N; b++) A[b] = s.nextInt();
            if (i < T - 1) s.nextLine();
            long ans = sumOfLengths(A, A.length);
            System.out.println(ans);
        }
    }

    static long sumOfLengths(int[] A, int n) {
        Set<Integer> set = new HashSet<>();
        int left = 0, right = 0;
        long ans = 0;
        while (left < n) {
            while (right < n && !set.contains(A[right])) {
                set.add(A[right]);
                right++;
            }
            long len = right - left;
            ans += (len * (len + 1)) / 2;
            set.remove(A[left]);
            left++;
        }
        return ans;
    }
}
