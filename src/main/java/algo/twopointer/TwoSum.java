package algo.twopointer;

/**
 * Problem:
 * Given a sorted array A, having N integers. You need to find any pair(i,j) having sum as given number X.
 *
 * Constraints:
 * Array A contains about 10^5 integers with each having values around 10^9
 */

public class TwoSum {

    public static void main(String[] args) {
        long[] a = {1,2,4,6,12,15,21};
        long sum = 9;
        boolean res = twoSum(a, sum);
        System.out.println(res);
    }

    public static boolean twoSum(long[] a, long sum) {
        int l = 0;
        int r = a.length - 1;
        while (l < r) {
            long s = a[l] + a[r];
            if (s == sum) return true;
            if (s > sum) r--;
            else l++;
        }
        return false;
    }
}
