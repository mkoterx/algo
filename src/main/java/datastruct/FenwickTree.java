package datastruct;

public class FenwickTree {

    private int[] tree;
    private int n;

    public FenwickTree(int[] arr) {
        construct(arr);
    }

    public void construct(int[] arr) {
        n = arr.length + 1;
        tree = new int[n];
        for (int i = 0; i < n; i++)
            tree[i] = 0;

        for (int i = 0; i < arr.length; i++)
            update(i, arr[i]);
    }

    public void update(int index, int val) {
        index = index + 1;
        while (index < n) {
            tree[index] += val;
            index += index & (-index);
        }
    }

    public int get(int index) {
        int res = 0;
        index = index + 1;
        while (index > 0) {
            res += tree[index];
            index -= index & (-index);
        }
        return res;
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < n; i++) sb.append(tree[i] + " ");
        return sb.toString();
    }

    public static void main(String[] args) {
        int[] arr = {2, 1, 1, 3, 2, 3, 4, 5, 6, 7, 8, 9};
        FenwickTree fTree = new FenwickTree(arr);
//        System.out.println(fTree.print());

        System.out.println("Sum of elements in arr[0..5] is "+ fTree.get(5));
//        arr[3] += 6;
//        fTree.update(3, 6);
//        System.out.println("Sum of elements in arr[0..5] is "+ fTree.get(5));

        System.out.println("Sum of elements in arr[2..5] is " + (fTree.get(5) - fTree.get(2-1)));
    }
}
