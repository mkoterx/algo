package algo.dp;

import static algo.dp.EditDistance.calc_min_edit_dist;
import static algo.dp.EditDistance.count_ways;

public class EditDistanceTest {

    public static void main(String[] args) {
        test_calc_min_edit_dist();
        test_count_ways();
    }

    static void test_calc_min_edit_dist() {
        System.out.println("=== Running tests for calc_min_edit_dist ===");
        char[] A;
        char[] B;
        int result;
        int expected;

        A = new char[]{'E', 'D', 'I', 'T', 'I', 'N', 'G'};
        B = new char[]{'D', 'I', 'S', 'T', 'A', 'N', 'C', 'E'};
        expected = 5;
        result = calc_min_edit_dist(A, B);
        if (result == expected)
            System.out.println(String.format("[%s] result: %d", LcsTest.STATUS.SUCCESS, result));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", LcsTest.STATUS.FAILED, expected, result));

        A = new char[]{'B', 'R', 'E', 'A', 'D',};
        B = new char[]{'R', 'E', 'A', 'L', 'L', 'Y'};
        expected = 4;
        result = calc_min_edit_dist(A, B);
        if (result == expected)
            System.out.println(String.format("[%s] result: %d", LcsTest.STATUS.SUCCESS, result));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", LcsTest.STATUS.FAILED, expected, result));
    }

    static void test_count_ways() {
        System.out.println("=== Running tests for count_ways ===");
        char[] A;
        char[] B;
        int result;
        int expected;

        A = new char[]{'E', 'D', 'I', 'T', 'I', 'N', 'G'};
        B = new char[]{'D', 'I', 'S', 'T', 'A', 'N', 'C', 'E'};
        expected = 2;
        result = count_ways(A, B);
        if (result == expected)
            System.out.println(String.format("[%s] result: %d", LcsTest.STATUS.SUCCESS, result));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", LcsTest.STATUS.FAILED, expected, result));

        A = new char[]{'B', 'R', 'E', 'A', 'D',};
        B = new char[]{'R', 'E', 'A', 'L', 'L', 'Y'};
        expected = 3;
        result = count_ways(A, B);
        if (result == expected)
            System.out.println(String.format("[%s] result: %d", LcsTest.STATUS.SUCCESS, result));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", LcsTest.STATUS.FAILED, expected, result));
    }
}
