package algo.dp;

import static algo.dp.Lcs.calc_lcs_len;
import static algo.dp.Lcs.calc_lcs_len_rec;

public class LcsTest {

    enum STATUS {
        SUCCESS, FAILED
    }

    public static void main(String[] args) {
        test_calc_lcs_len();
        test_calc_lcs_len_rec();
    }

    static void test_calc_lcs_len() {
        System.out.println("=== Running tests for calc_lcs_len ===");
        char[] A; char[] B;
        int result;
        int expected;

        A = new char[]{'A', 'B', 'C', 'B', 'D', 'A', 'B'};
        B = new char[]{'A', 'B', 'D', 'C', 'B', 'A', 'B', 'B'};
        expected = 6;
        result = calc_lcs_len(A, B);
        if (result == expected)
            System.out.println(String.format("[%s] result: %d", STATUS.SUCCESS, result));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", STATUS.FAILED, expected, result));

        A = new char[]{'A', 'B', 'C', 'B', 'D', 'A', 'B'};
        B = new char[]{'B', 'D', 'C', 'B', 'A', 'B', 'A'};
        expected = 5;
        result = calc_lcs_len(A, B);
        if (result == expected)
            System.out.println(String.format("[%s] result: %d", STATUS.SUCCESS, result));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", STATUS.FAILED, expected, result));

        A = new char[]{'A', 'B', 'C', 'B', 'D', 'A', 'B'};
        B = new char[]{'B', 'D', 'C', 'A', 'B', 'A'};
        expected = 4;
        result = calc_lcs_len(A, B);
        if (result == expected)
            System.out.println(String.format("[%s] result: %d", STATUS.SUCCESS, result));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", STATUS.FAILED, expected, result));
    }

    static void test_calc_lcs_len_rec() {
        System.out.println("=== Running tests for calc_lcs_len_rec ===");
        char[] A; char[] B;
        int actual;
        int expected;

        A = new char[]{'A', 'B', 'C', 'B', 'D', 'A', 'B'};
        B = new char[]{'A', 'B', 'D', 'C', 'B', 'A', 'B', 'B'};
        expected = 6;
        actual = calc_lcs_len_rec(A, B);
        if (actual == expected)
            System.out.println(String.format("[%s] result: %d", STATUS.SUCCESS, actual));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", STATUS.FAILED, expected, actual));

        A = new char[]{'A', 'B', 'C', 'B', 'D', 'A', 'B'};
        B = new char[]{'B', 'D', 'C', 'B', 'A', 'B', 'A'};
        expected = 5;
        actual = calc_lcs_len_rec(A, B);
        if (actual == expected)
            System.out.println(String.format("[%s] result: %d", STATUS.SUCCESS, actual));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", STATUS.FAILED, expected, actual));

        A = new char[]{'A', 'B', 'C', 'B', 'D', 'A', 'B'};
        B = new char[]{'B', 'D', 'C', 'A', 'B', 'A'};
        expected = 4;
        actual = calc_lcs_len_rec(A, B);
        if (actual == expected)
            System.out.println(String.format("[%s] result: %d", STATUS.SUCCESS, actual));
        else
            System.out.println(String.format("[%s] expected: %d, actual: %d", STATUS.FAILED, expected, actual));
    }
}
